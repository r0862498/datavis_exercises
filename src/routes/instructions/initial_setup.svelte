<script context="module">
  export const hydrate = false;
</script>

<script>
  import CC from "$lib/ui/CodeInline.svelte";
  import Code from "$lib/ui/CodeBlock.svelte";
  import Img from "$lib/ui/Image.svelte";
</script>

<h1>Initial setup</h1>
<p>
  There are a couple of things you have to do to create your own development
  environment to work on this website. We will describe these steps in detail
  here.
</p>

<h5>GitLab</h5>

<p>
  To access the source code you will be working with, you have to create a <a
    class="link-secondary"
    target="_blank"
    href="https://gitlab.com/users/sign_up?test=capabilities">GitLab account</a
  >. We recommend that you create an account with GitLab directly, rather than
  sign up via Google or any of the other listed options, as our instructions
  will not cover the steps you'd have to perform for those type of accounts!
</p>
<p>
  GitLab will ask you a couple of questions when you are signing up, you can
  fill these in as shown in the image. Afterwards, you should see the welcome
  screen.
</p>
<div class="d-flex flex-row justify-content-around">
  <Img src="/img/gitlab_signup.png" width="300" caption="Sign up questions" />
  <Img
    src="/img/gitlab_welcome_screen.png"
    width="400"
    caption="Welcome screen"
  />
</div>
<p>
  Once you have created your account, go to this
  <a
    class="link-secondary"
    target="_blank"
    href="https://gitlab.com/vda-lab/datavis_exercises">repository</a
  >
  and click on the <CC code="fork" /> button. <strong>Important:</strong> Make
  sure your fork is public and the repsitory URL is directly under your
  username! Click the <CC code="Fork" /> button to create your fork.
</p>
<div class="d-flex flex-row justify-content-around">
  <Img
    src="/img/gitlab_fork_repo.png"
    width="350"
    caption="1. Click the fork button"
  />
  <Img
    src="/img/gitlab_fork.png"
    width="350"
    caption="2. Set your fork to public under your account"
  />
</div>

<h5>Vercel</h5>
<p>
  We use <a
    class="link-secondary"
    target="_blank"
    href="https://vercel.com/signup">Vercel</a
  >
  as hosting provider. Make a vercel account that is linked to your GitLab account
  by clicking the <CC code="Continue with GitLab" /> button as shown in the image
  below. This will pop up a new window, where you have to click the <CC
    code="authorize"
  /> button.
</p>
<div class="d-flex flex-row justify-content-around">
  <Img
    src="/img/vercel_signup.png"
    width="300"
    caption="1. Vercel sign up"
  /><Img
    src="/img/vercel_signup_2.png"
    width="300"
    caption="2. Authorize account"
  />
</div>
<p>
  Now you are logged in, and can click the click the <CC code="Import" /> button
  to import your fork of the <CC code="datavis_exercises" /> repository.
  <strong>Important:</strong> prefix the project name with you student number: <CC
    code="[studentnumber]-datavis-exercises"
  /> (remove the square brackets when filling in your student number). Then, click
  the <CC code="Deploy" /> button.
</p>
<div class="d-flex flex-row justify-content-around">
  <Img
    src="/img/vercel_import.png"
    width="300"
    caption="1. Import your fork"
  /><Img
    src="/img/vercel_import_2.png"
    width="300"
    caption="2. Change the project name"
  />
</div>
<p>
  Once Vercel is done building the website, click the <CC
    code="Go To Dashboard"
  /> button. You should now be able to browse to <CC
    code="https://[studentnumber]-datavis-exercises.vercel.app"
  />
  and see your version of this website!
</p>

<h5>Installing Git, VSCode, and Nodejs</h5>

<p>
  In this course, we will build custom visualizations using javascript, we
  recommend you use <a
    target="_blank"
    class="link-secondary"
    href="https://code.visualstudio.com/download">Visual Studio Code</a
  >
  as a text-editor for this course. For the installation, you can just accept the
  default settings. Once VSCode is installed and open, also install the
  <a
    target="_blank"
    class="link-secondary"
    href="https://marketplace.visualstudio.com/items?itemName=svelte.svelte-VSCode"
    >Svelte for VS Code</a
  >
  extension to get proper syntax highlighting and navigate to the settings page to
  enable the <CC code="Extensions > Git > Allow Force Push" /> option:
</p>
<div class="d-flex flex-row justify-content-around">
  <Img
    src="/img/vs_code_extensions.png"
    width="200"
    caption="1. Install svelte extension."
  />
  <Img
    src="/img/vscode_enable_force_push.png"
    width="400"
    caption="2. Enable git force-push."
  />
</div>

<p>
  To access your repository on your machine, you'll have to install git. On
  linux git is probably already installed and if not, just use your package
  manager to install it. On Mac, Xcode ships with git so you may already have it
  installed. If not, you can install <a
    class="link-secondary"
    href="https://brew.sh/"
    target="_blank">Home Brew</a
  >
  (see their instructions) and then install git via: <CC
    language="shell"
    code="$: brew install git"
  />. On Windows, you have to download and install
  <a
    class="link-secondary"
    target="_blank"
    href="https://git-scm.com/download/win">git from this link</a
  >. For the most part you can just accept the default values, but make sure to
  set the default editor to VSCode and adjust your path so you can use git in
  3rd-party software, as shown in the images below.
</p>
<div class="d-flex flex-row justify-content-around">
  <Img
    src="/img/git_install_editor.png"
    width="300"
    caption="Set the default editor with git"
  />
  <Img
    src="/img/git_install_path.png"
    width="300"
    caption="Add git to the path"
  />
</div>
<p>
  If you are on Windows, close and re-open the visual studio code editor.
  Navigate to the settings page and set the <CC
    code="Terminal > Integrated > Default Profile > Windows"
  /> option to
  <CC code="Git Bash" />. Now you can access a bash terminal from within VSCode
  as shown in the image below!
</p>
<div class="d-flex flex-row justify-content-around">
  <Img
    src="/img/vs_code_settings.png"
    width="400"
    caption="1. Set default terminal profile"
  />
  <Img
    src="/img/vs_code_terminal.png"
    width="400"
    caption="2. Access VSCode's terminal"
  />
</div>
<p>
  Now, we have to tell git who we are by running two commands in VSCode's
  terminal. In these commands, fill in your name and email in the square
  brackets and remove the brackets. The second image above shows how you can
  access VSCode's terminal.
</p>
<Code
  language="shell"
  code={`$: git config --global user.name "[your name]"
$: git config --global user.email "[your email]"
`}
/>
<p>
  Finally, you'll need to install <a
    class="link-secondary"
    target="_blank"
    href="https://nodejs.org/en/download/">nodejs</a
  >
  to work with javascript. See their website for installation instructions. You can
  just use their default installation options. If you are familiar with
  <a
    class="link-secondary"
    target="_blank"
    href="https://docs.conda.io/en/latest/miniconda.html">conda</a
  >, you can also use that to create an environment with nodejs.
</p>

<h5>Cloning the repository</h5>
<p>
  To work on the exercises, you have to clone your fork of the website. This
  will make the code available on your computer. Open VSCode, click the <CC
    code="File / Open Folder"
  /> button in the menu-bar and navigating to the folder where you want to store
  this course's files (don't put the files in a local synchronization of Google Drive,
  npm will break if you do). Then, open the VSCode's terminal as shown in the image
  below:
</p>
<Img
  src="/img/vs_code_terminal.png"
  width="400"
  caption="Acces VSCode's terminal"
/>
<p>
  To get the correct URL for cloning, you can click the <CC code="Clone" />
  button on your fork's GitLab page; use the https-URL:
</p>
<Code
  language="shell"
  code="$: git clone https://gitlab.com/[username]/datavis_exercises.git"
/>
<p>
  Now, there should be a folder called <CC code="datavis_exercises" />. From
  this point onward, all work for this course is performed within the <CC
    code="datavis_exercises"
  /> folder. After you have opened the folder in VSCode once, you can easily find
  it again under the <CC code="File / Open Recent" /> menu. The first time you open
  the folder, VSCode will ask you if you trust the contents, which you can safely
  accept.
</p>

<h5>Installing dependencies</h5>
<p>
  The last step in getting started is installing the project's dependencies.
  Open the <CC code="datavis_exercises" /> folder in VSCode, open a VSCode's terminal
  and run:
</p>
<Code language="shell" code="$: npm install" />
<p>
  If you get an error saying that npm cannot find a <CC code="package.json" /> file,
  then you have not opened the <CC code="datavis_exercises" /> folder in VSCode directly.
  Go to <CC code="File / Open Folder" /> in the VSCode's menubar and navigate to
  the
  <CC code="datavis_exercises" /> folder. Now <CC
    language="shell"
    code="$: npm install"
  /> should work without issue.
</p>
<Img src="/img/npm_error.png" width="500," caption="Possible npm error." />
<p>
  To receive the updates that we will release with each week's instructions, you
  have to tell git where the original repository lives. Add a remote called <CC
    code="upstream"
  /> pointing to <CC code="https://gitlab.com/vda-lab/datavis_exercises" /> following
  the steps in the images below:
</p>
<div class="d-flex flex-row justify-content-around">
  <Img
    src="/img/vscode_add_remote_1.png"
    width="400"
    caption="1. Add the remote"
  />
  <div>
    <Img
      src="/img/vscode_add_remote_2.png"
      width="400"
      caption="2. Fill in the URL and press enter"
    />
    <Img
      src="/img/vscode_add_remote_3.png"
      width="400"
      caption="3. Give the remote a name and press enter"
    />
  </div>
</div>
<p>
  Now, VSCode will ask if it should automatically fetch from the remote, which
  you can accept:
</p>
<Img
  src="/img/vscode_add_remote_4.png"
  width="300"
  caption="Enable periodic fetch"
/>
<p>
  <strong>Important:</strong> Finaly, open the file <CC
    code="src/routes/__layout.svelte"
  /> and change lines 13 to 15 with your name, student number and the URL of your
  fork:
</p>
<Code
  language="svelte"
  code={`...
  <Footer
    name="Your Name"
    number="your student number"
    url="https://gitlab.com/[your_username]/datavis_exercises"
  />
...`}
/>

<h5>The end</h5>
<p>You should now:</p>
<ol>
  <li>
    Have local copy of the code for this website that will be used for the
    exercises.
  </li>
  <li>
    Your name and student number should be listed in the footer of the website.
  </li>
</ol>
<p>
  Continue with the <a
    class="link-secondary"
    href="/instructions/working_on_exercises"
  >
    working on the exercises
  </a> instructions!
</p>
